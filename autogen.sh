#
# Requires: >= automake 1.9, >= autoconf 2.61
# Conflicts: autoconf 2.13
set -e

# Refresh GNU autotools toolchain.
echo Cleaning autotools files...
find -type d -name autom4te.cache -print0 | xargs -0 rm -rf \;
find -type f \( -name missing -o -name install-sh -o -name mkinstalldirs \
	-o -name depcomp -o -name ltmain.sh -o -name configure \
	-o -name config.sub -o -name config.guess \
	-o -name Makefile.in \) -print0 | xargs -0 rm -f

# because we set ACLOCAL_AMFLAGS = -I m4 in Makefile.am
mkdir -p m4

echo Running autoreconf ...
autoupdate
autoreconf --force --install

# Not yet i18n support from GNU gettext.
# autopoint --force

# For the Debian package build
test -d debian && {
	# link these in Debian builds
	rm -f config.sub config.guess
	ln -s /usr/share/misc/config.sub .
	ln -s /usr/share/misc/config.guess .
}

exit 0
